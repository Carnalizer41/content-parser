package com.shyrkov.enums;

public enum Language {
    ENGLISH, RUSSIAN, OTHER
}
