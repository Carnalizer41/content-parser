package com.shyrkov.services;

import com.shyrkov.Word;
import com.shyrkov.entities.SongStatisticEntity;
import com.shyrkov.exceptions.TextAnalyzerServiceException;

import java.util.Map;

//пришла мапа, берем каждое слово, вызываем ГетАмоунт и смотрим сколько раз слово повторяется и сумируем
public class AmountAllWords implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {
        if(songStatisticEntity==null){
            throw new TextAnalyzerServiceException("SongStatistic can`t be null");
        }
        if(stringWordMap == null){
            throw new TextAnalyzerServiceException("Word map can`t be null");
        }
        Integer number = stringWordMap.values()
                                                .stream()
                                                .mapToInt(Word::getAmount)
                                                .sum();
        songStatisticEntity.setTotalWordsAmount(number);
        return songStatisticEntity;
    }
}
