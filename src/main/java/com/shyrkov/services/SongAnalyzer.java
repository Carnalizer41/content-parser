package com.shyrkov.services;

import com.shyrkov.SongInfo;
import com.shyrkov.Word;
import com.shyrkov.entities.SongStatisticEntity;
import com.shyrkov.exceptions.TextAnalyzerServiceException;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SongAnalyzer {

    private List<WordFunction> wordFunctionList;
    private WordExtractor wordExtractor;

    public SongAnalyzer(List<WordFunction> wordFunctionList, WordExtractor wordExtractor) {
        this.wordFunctionList = wordFunctionList;
        this.wordExtractor = wordExtractor;
    }

    public List<SongStatisticEntity> analyzeSong(List<SongInfo> songInfo) {
        List<SongStatisticEntity> songStatisticEntities = new ArrayList<>();//будем хранить сонг статистик ентити
        if(songInfo == null){
            throw new TextAnalyzerServiceException("SongInfo list can`t be null");
        }
        if(wordFunctionList == null){
            throw new TextAnalyzerServiceException("WordFunction list can`t be null");
        }
        if(wordExtractor == null){
            throw new TextAnalyzerServiceException("WordExtractor can`t be null");
        }
        for (SongInfo info : songInfo) {
            SongStatisticEntity songStatisticEntity = new SongStatisticEntity();
            Map<String, Word> wordMap = wordExtractor.getWords(info);
            for (WordFunction wordFunction : wordFunctionList) {
                wordFunction.apply(wordMap, songStatisticEntity);
            }
            songStatisticEntity.setDate(Calendar.getInstance().getTime());
            songStatisticEntity.setTimestamp(System.currentTimeMillis());
            songStatisticEntity.setContentUrl(info.getUrl());
            songStatisticEntity.setContent(String.join("\n", info.getLines()));
            songStatisticEntities.add(songStatisticEntity);
        }
        return songStatisticEntities;
    }
}
