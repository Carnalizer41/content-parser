package com.shyrkov.services;

import com.shyrkov.exceptions.TextAnalyzerServiceException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class WebClient {
    public Document getPage(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (Exception e) {
            throw new TextAnalyzerServiceException("Error...", e);
        }
    }
}


