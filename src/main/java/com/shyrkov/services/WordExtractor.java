package com.shyrkov.services;

import com.shyrkov.SongInfo;
import com.shyrkov.Word;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WordExtractor {

    public Map<String, Word> getWords(SongInfo songInfo) {// брать строчки с песни и раскладывать слова
        Map<String, Word> stringWordMap = new HashMap<>();
        for (String line : songInfo.getLines()) {
            line = line.toLowerCase(); // делаем маленькими буквами
            for (String word : line.split("[!?:\\-.,\\s]+")) {// из стоски выдергиваем слово
                if (word.equals("")) continue; //отсеиваем чтобы пустышки не считало словами
                if (stringWordMap.get(word) == null) {
                    stringWordMap.put(word, new Word(word));
                } else stringWordMap.get(word).count();
            }
        }
        return stringWordMap;
    }

}
