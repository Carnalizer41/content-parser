package com.shyrkov.services;

import com.shyrkov.Word;
import com.shyrkov.entities.SongStatisticEntity;

import java.util.Map;

public interface WordFunction {
    SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity);

}
