create TABLE IF NOT EXISTS user_role
(
    id SERIAL PRIMARY KEY,
    role VARCHAR(1024) NOT NULL,
    user_id BIGINT not NULL,
    FOREIGN KEY(user_id) REFERENCES users(id)
);

insert into users (email, password, login)
values ('gook.goro@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Pupok');

with super_admin_user_id as (select currval('users_id_seq'))
insert into user_role(role, user_id)
values ('SUPER_ADMIN', (select * from super_admin_user_id));