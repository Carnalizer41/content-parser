CREATE TABLE IF NOT EXISTS "statistic"
(
    id                          SERIAL PRIMARY KEY,
    "date"                      DATE,
    "timestamp"                 BIGINT,
    content_url                 VARCHAR(256),
    "content"                   TEXT,
    total_number_of_words       INT,
    number_of_same_words        INT,
    number_of_unique_words      INT,
    most_popular_word           VARCHAR(50)
);
