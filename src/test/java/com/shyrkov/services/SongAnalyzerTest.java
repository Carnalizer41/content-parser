package com.shyrkov.services;

import com.shyrkov.SongInfo;
import com.shyrkov.entities.SongStatisticEntity;
import com.shyrkov.exceptions.TextAnalyzerServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SongAnalyzerTest {

    private SongAnalyzer songAnalyzer;
    @Mock
    private WordFunction wordFunction;
    @Mock
    private WordExtractor wordExtractor;

    @Before
    public void setUp() {
        List<WordFunction> wordFunctionList = new ArrayList<>();
        wordFunctionList.add(wordFunction);
        songAnalyzer = new SongAnalyzer(wordFunctionList, wordExtractor);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void exceptionWillBeThrownWhenSongInfoListIsNull() {
        songAnalyzer.analyzeSong(null);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void exceptionWillBeThrownWhenWordFunctionListIsNull() {
        SongAnalyzer songAnalyzer = new SongAnalyzer(null, wordExtractor);
        List<SongInfo> songInfoList = new ArrayList<>();
        songInfoList.add(new SongInfo("asd", new ArrayList<>(), "fdas"));

        songAnalyzer.analyzeSong(songInfoList);
    }

    @Test(expected = TextAnalyzerServiceException.class)
    public void exceptionWillBeThrownWhenWordExtractorIsNull() {
        SongAnalyzer songAnalyzer = new SongAnalyzer(new ArrayList<>(), null);
        List<SongInfo> songInfoList = new ArrayList<>();
        songInfoList.add(new SongInfo("asd", new ArrayList<String>(), "fdas"));

        songAnalyzer.analyzeSong(songInfoList);
    }

    @Test
    public void wordsWillBeExtractedFromSongInfoList() {
        List<SongInfo> songInfoList = new ArrayList<>();
        songInfoList.add(new SongInfo("asd", new ArrayList<String>(), "fdas"));

        songAnalyzer.analyzeSong(songInfoList);

        Mockito.verify(wordExtractor).getWords(songInfoList.get(0));
    }
}